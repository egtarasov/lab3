using System.Text.Json;
using Lab3.Models;
using StackExchange.Redis;
using Xunit.Abstractions;

namespace TaxiStatistic;

public class UnitTest1
{
    private readonly ITestOutputHelper _testOutputHelper;

    public UnitTest1(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
    }

    [Fact]

    public async Task Test1()
    {
        var connectionRedis = ConnectionMultiplexer.Connect("localhost");
        var redisDb = connectionRedis.GetDatabase();
        var t = new TaxiRidesStatisticService("Server=localhost;Port=5432;Userid=postgres;Password=postgres;Database=postgres");
        
        _testOutputHelper.WriteLine(JsonSerializer.Serialize(await t.Haversine(redisDb, 100)));
    }
}