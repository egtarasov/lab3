using System.Text.Json;
using Dapper;
using Microsoft.Extensions.Options;
using Npgsql;
using StackExchange.Redis;
using PostgreSQLCopyHelper;


namespace Lab3.Models;

public interface ITaxiRidesStatisticService
{
    public Task<List<TaxiRide>> ReadCsv(string filePath);

    public Task AddRecords(IEnumerable<TaxiRide> entities);

    public Task<List<HourInfo>> CreateHourStatistic(IDatabase redisDb);

    public Task<List<double>> Haversine(IDatabase redisDb, uint limit);
}

public class TaxiRidesStatisticService : ITaxiRidesStatisticService
{

    private NpgsqlConnection _con;

    public static readonly JsonSerializerOptions JsonOptions = new()
    {
        WriteIndented = true
    };

    public TaxiRidesStatisticService(
        IOptions<PostgresOptions> postgresOptions
    )
    {
        _con = new NpgsqlConnection(postgresOptions.Value.ConnectionString);
    }
    
    public async Task<List<TaxiRide>> ReadCsv(string filePath)
    {
        try
        {
            using var reader = new StreamReader(filePath);
            var start = true;
            var listOfRides = new List<TaxiRide>();
            
            while ((await reader.ReadLineAsync())! is { } line)
            {
                line = line.Replace("\"", "");
                var currentRow = line.Split(',');

                if (start)
                {
                    start = false;
                    continue;
                }

                var i = 2;
                var record = new TaxiRide()
                {
                    VendorId = currentRow[i++],
                    PickupDatetime = Convert.ToDateTime(currentRow[i++]),
                    DropoffDatetime = Convert.ToDateTime(currentRow[i++]),
                    PassangerCount = int.Parse(currentRow[i++]),
                    DayOfWekk = ParseDayOfWeek(currentRow[i++]),
                    PickupLongitude = double.Parse(currentRow[i++]),
                    PickupLattiude = double.Parse(currentRow[i++]),
                    DropoffLongitude = double.Parse(currentRow[i++]),
                    DropoffLatitude = double.Parse(currentRow[i++]),
                    StoreAndFwdFlag = ParseFlag(currentRow[i++]),
                    TripDuration = int.Parse(currentRow[i])
                };

                listOfRides.Add(record);
            }
            
            return listOfRides;
        }
        catch (Exception)
        {
            throw new ArgumentException("Cant process a file");
        }
    }

    public async Task AddRecords(IEnumerable<TaxiRide> entities)
    {
        await _con.OpenAsync();

        var copyHelper = new PostgreSQLCopyHelper<TaxiRide>("public", "taxi_trip_duration")
            .MapVarchar("vendor_id", x => x.VendorId)
            .MapTimeStamp("pickup_datetime", x => x.PickupDatetime)
            .MapTimeStamp("dropoff_datetime", x => x.DropoffDatetime)
            .MapInteger("passenger_count", x => x.PassangerCount)
            .MapDouble("pickup_longitude", x => x.PickupLongitude)
            .MapInteger("day_of_week_id", x => x.DayOfWekk)
            .MapDouble("pickup_latitude", x => x.PickupLattiude)
            .MapDouble("dropoff_longitude", x => x.DropoffLongitude)
            .MapDouble("dropoff_latitude", x => x.DropoffLatitude)
            .MapInteger("store_and_fwd_flag", x => x.StoreAndFwdFlag)
            .MapInteger("trip_duration", x => x.TripDuration);

        await copyHelper.SaveAllAsync(_con, entities);
    }

    public async Task<List<HourInfo>> CreateHourStatistic(IDatabase redisDb)
    {
        var command =
            @"
            select
                extract(hour from pickup_datetime) as hour,
                count(*) as quantity
            from public.taxi_trip_duration
            group by hour
            order by hour;
            ";
        var hourStats =
            (await _con
                .QueryAsync<HourInfo>(command))
            .ToList();

        await redisDb.StringSetAsync(
            "hourStatistic", 
            JsonSerializer.Serialize(
                hourStats, 
                JsonOptions),
            new TimeSpan(0, 15, 0));
        
        return hourStats.ToList();
    }

    public async Task<List<double>> Haversine(IDatabase redisDb, uint limit)
    {
        var command =
            @$"
            select Haversine(
                pickup_latitude,
                pickup_longitude,
                dropoff_latitude,
                dropoff_longitude) 
                from taxi_trip_duration
                limit {limit};
            ";
        
        var haversine =  (await _con
            .QueryAsync<double>(command))
            .ToList();

        await redisDb
            .StringSetAsync(
                "haversine",
                JsonSerializer.Serialize(haversine, JsonOptions),
                new TimeSpan(0, 15, 0));

        return haversine;
    }
    
    private static int ParseFlag(string flag) => 1;
    
    private static int ParseDayOfWeek(string day) => day switch
    {
        "Monday" => 1,
        "Tuesday" => 2,
        "Wednesday" => 3,
        "Thursday" => 4,
        "Friday" => 5,
        "Saturday" => 6,
        "Sunday" => 7,
        _ => 1
    };

}