namespace Lab3.Models;

public class TaxiRide
{
    public int Id { get; set; }

    public string VendorId { get; set; }

    public DateTime PickupDatetime { get; set; }

    public DateTime DropoffDatetime { get; set; }
    
    public int PassangerCount { get; set; }
    
    public int DayOfWekk { get; set; }
    
    public double PickupLongitude { get; set; }
    
    public double PickupLattiude { get; set; }
    
    public double DropoffLongitude { get; set; }
    
    public double DropoffLatitude { get; set; }
    
    public int StoreAndFwdFlag { get; set; }
    
    public int TripDuration { get; set; }
}