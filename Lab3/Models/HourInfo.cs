namespace Lab3.Models;

public class HourInfo
{
    public int Hour { get; set; }
    
    public int Quantity { get; set; }
}