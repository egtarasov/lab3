using System.Text.Json;
using Lab3.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using StackExchange.Redis;


namespace Lab3.Controllers;

[ApiController]
[Route("[controller]")]
public class DatabaseController : ControllerBase
{
    private readonly ILogger<DatabaseController> _logger;

    private readonly IDatabase _redisDb;

    private readonly ITaxiRidesStatisticService _statisticService;
    
    public DatabaseController(
        ITaxiRidesStatisticService taxiRidesStatisticService,
        ILogger<DatabaseController> logger)
    {
        
        _logger = logger;
        var connectionRedis = ConnectionMultiplexer.Connect("localhost");
        _redisDb = connectionRedis.GetDatabase();
        _statisticService = taxiRidesStatisticService;
    }

    [HttpGet("get_distance")]
    public async Task<ActionResult> GetDistance(uint limit = 100)
    {
        var cachedObject = _redisDb.StringGet("haversine");
        var result = cachedObject.IsNull
            ? await _statisticService.Haversine(_redisDb, limit)
            : JsonSerializer.Deserialize<List<double>>(cachedObject.ToString());
        
        return base.Ok(JsonSerializer
            .Serialize(result,
                new JsonSerializerOptions()
                {
                    WriteIndented = true
                }));
    }

    [HttpGet("get_hour_statistic")]
    public async Task<ActionResult> GetHourStatistic()
    {
        var cachedObject = _redisDb.StringGet("hourStatistic");
        var result = cachedObject.IsNull
            ? await _statisticService.CreateHourStatistic(_redisDb)
            : JsonSerializer.Deserialize<List<HourInfo>>(cachedObject.ToString());
        
        return base.Ok(JsonSerializer
            .Serialize(result,
                new JsonSerializerOptions()
                {
                    WriteIndented = true
                }));
    }
    
    [HttpPut("initialize")]
    public async Task<ActionResult> UpdateDatabase()
    {
        try
        {
            var listOfRecords =
                await _statisticService
                    .ReadCsv("../Migrations/Taxi.csv");

            await _statisticService.AddRecords(listOfRecords);
        }
        catch (ArgumentException ex)
        {
            _logger.LogInformation("Argument exception");
            return base.Conflict(ex.Message);
        }
        catch (Exception ex)
        {
            _logger.LogCritical("Critical error");
            return base.Problem("Unexpected outcome");
        }
        
        return base.Ok("Database have been updated");
    }
}