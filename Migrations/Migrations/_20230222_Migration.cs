using FluentMigrator;

namespace Migrations.Migrations;

public class _20230222_Migration: Migration
{
    private const string scriptUp =
        @"
       CREATE OR REPLACE FUNCTION Haversine(
                        lat1 double precision,
                    long1 double precision,
                    lat2 double precision,
                    long2 double precision) 
                    RETURNS DOUBLE PRECISION 
                LANGUAGE plpgsql AS $$
                declare 
                    a double precision;
                    c double precision;
                    d double precision;
                    x1 double precision;
                    x2 double precision;
                    y1 double precision;
                    y2 double precision;
                BEGIN
                    x1 = lat1 *  pi()/180;
                    x2 = lat2 *  pi()/180;
                    y1 = long1 *  pi()/180;
                    y2 = long2 *  pi()/180;
                    
                    a = power(sin((x2-x1)/2), 2) + cos(x1)*cos(x2)*power(sin((y2-y1)/2), 2);
                    c = 2 * atan2(sqrt(a), sqrt(1-a));
                    d = 6371 * 1000  * c;
                    return d;
                END
                $$;
        ";

    private const string scriptDown = 
        @"
        DELETE FUNCTION Haversine;
        ";


    public override void Up()
    {
        Execute.Sql(scriptUp);
    }

    public override void Down()
    {
    Execute.Sql(scriptDown);
    }
}