using FluentMigrator;

namespace Migrations.Migrations;

[Migration(20230217, "Create tablees")]
public class _20230217_Migration: Migration
{

    private const string scriptUp =
        @"
        --create schema public;

        create table public.taxi_trip_duration (
            id serial primary key constraint pk_id_taxi_trip_duration not null AUTO_INCREMENT,
            vendor_id character varying(30),
            pickup_datetime timestamp without time zone not null,
            dropoff_datetime timestamp without time zone not null,
            passenger_count integer not null,
            day_of_week_id integer not null,
            pickup_longitude double precision not null,
            pickup_latitude double precision not null,
            dropoff_longitude double precision not null,
            dropoff_latitude double precision not null,
            store_and_fwd_flag integer not null,
            trip_duration integer not null
            );
        
        create table public.days_of_week (
          day_of_week_id serial not null,
          day_of_week varchar(10)
        );

        insert into public.days_of_week values
               (1, 'Monday'),
               (2,'Tuesday'),
               (3,'Wednesday'),
               (4,'Thursday'),
               (5,'Friday'),
               (6,'Saturday'),
               (7,'Sunday');
    create table public.store_and_fwd_flags(
        store_and_fwd_flag_id serial not null,
        store_and_fwd_flag varchar(10)
    );

    insert into public.store_and_fwd_flags values
        (1, 'N');        
";

    private const string scriptDown =
        @"
        drop table public.taxi_trip_duration;
        drop table public.days_of_week;
        drop table public.store_and_fwd_flags;
        drop schema public;
        ";

            public override void Up()
    {
        Execute.Sql(scriptUp);
    }

    public override void Down()
    {
        Execute.Sql(scriptDown);
    }
}